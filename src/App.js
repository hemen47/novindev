import React, {useEffect, useState, Fragment} from "react";
import {Route, Switch} from "react-router";
import './App.css';
import Users from './container/Users';
import UserDetails from './container/UserDetails';
import Login from "./container/Login";
import Modal from "@material-ui/core/Modal";
import Error from "./container/Error";
import {CircularProgress} from "@material-ui/core";
import Header from "./container/Header";


function App() {

    const [openModal, setOpenModal] = useState(false);
    const [errorMessage, setErrorMessage] = useState(null);
    const [isLoading, setIsLoading] = useState(false);
    const [loggedIn, setLoggedIn] = useState(false);

    useEffect(() => {
        if (localStorage.getItem('token')) {
            setLoggedIn(true);
        }
    }, [loggedIn])


    const showError = (err) => {
        setErrorMessage(err.response.data.error);
        setOpenModal(true);
    };
    const hideError = () => {
        setErrorMessage(null);
        setOpenModal(false);
    };

    const showSpinner = () => {
        setIsLoading(true);
    };

    const hideSpinner = () => {
        setIsLoading(false);
    };

    const logIn = (token) => {
        localStorage.setItem('token', token);
        setLoggedIn(true);
    };

    const logOut = () => {
        localStorage.removeItem('token');
        setLoggedIn(false);
    };

    return (
        <div className="app">
            {!loggedIn ?
                <Login showError={showError} showSpinner={showSpinner} hideSpinner={hideSpinner} logIn={logIn}/> :
                <Fragment>
                    <Header logOut={logOut}/>
                    <Switch>
                        <Route exact path="/">
                            <Users showError={showError} showSpinner={showSpinner} hideSpinner={hideSpinner}/>
                        </Route>
                        <Route path="/:id">
                            <UserDetails/>
                        </Route>
                    </Switch>
                </Fragment>
            }

            {/*error modal*/}
            <Modal open={openModal} onClose={hideError}>
                <div>
                    <Error errorMessage={errorMessage} hideError={hideError}/>
                </div>
            </Modal>

            {/*spinner*/}
            {isLoading && <CircularProgress className='spinner'/>}
        </div>
    );
}

export default App;
