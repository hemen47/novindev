import React, {useEffect, useState} from "react";
import {useHistory} from "react-router-dom";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles({
    root: {
        display: "flex",
        flexDirection: "column",
        alignItems: "center",
    },
    details: {
        display: "flex",
        flexDirection: 'column',
        width: '300px',
        margin: '20px',
        border: '1px solid aqua',
        padding: '30px',
        alignItems: 'center',
        borderRadius: '8px',
    }
})

function UserDetails() {
    const history = useHistory();
    const [selectedUser, setSelectedUser] = useState(null);
    const classes = useStyles();

    useEffect(() => {
        if (history.location.state) {
            setSelectedUser(history.location.state);
        }
    }, [history])
    return (
        <div className={classes.root}>
            <div className={classes.details}>
                <h2>Profile Details:</h2>
                <img src={selectedUser && selectedUser.avatar} alt="avatar"/>
                <p>{selectedUser && selectedUser.first_name + ' ' + selectedUser.last_name}</p>
                <p>{selectedUser && selectedUser.email}</p>
                <p><b> Biography:</b> Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor
                    incididunt
                    ut labore et dolore magna aliqua. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui
                    officia deserunt mollit anim id est laborum.</p>
            </div>
            <Button onClick={() => history.push('/')}>Go Back</Button>

        </div>
    );
}

export default UserDetails;
