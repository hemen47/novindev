import React from "react";
import './user.css'

function User({user, onClick}) {

    return (
        <div onClick={onClick} className="user">
            <div className="image">
                <img src={user.avatar} alt="avatar"/>
            </div>
            <div className="info">
                <p>#: {user.id}</p>
                <p>{user.first_name + ' ' + user.last_name}</p>
                <p>{user.email}</p>
            </div>
        </div>
    );
}

export default User;
