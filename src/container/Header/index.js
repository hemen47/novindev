import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import Button from "@material-ui/core/Button";

const useStyles = makeStyles({
    root: {
        display: "flex",
        justifyContent: "space-around",
        boxShadow: '0px 9px 14px -10px rgba(0,0,0,0.75)',
    }

})

function Header({logOut}) {
    const classes = useStyles();
    return (
        <div className={classes.root}>
            <p>Welcome Dear...</p>
            <Button color="secondary" onClick={logOut}>logout</Button>
        </div>
    );
}

export default Header;
