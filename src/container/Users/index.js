import React, {useEffect, useState} from "react";
import axios from "axios";
import "./users.css";
import User from "../User";
import {useHistory} from "react-router-dom";


function Users({showSpinner, hideSpinner, showError}) {

    const [users, setUsers] = useState([]);
    const [page, setPage] = useState(1);
    const history = useHistory();

    const getUsers = () => {
        showSpinner();
        axios.get(`https://reqres.in/api/users?page=${page}`)
            .then((res) => {
                setUsers(res.data);
                hideSpinner();
            })
            .catch((err) => {
                showError(err);
                hideSpinner();
            })
    }

    useEffect(() => {
        getUsers();
    }, [page]);

    const changePage = (num) => {
        setPage(num);
    };

    return (
        <div className="users">
            <div className="container">
                {users.data && users.data.map((usr, i) => {
                    return <User key={i} onClick={() => history.push(`/${usr.id}`, usr)} user={usr}/>
                })}
            </div>
            <div className="pageNumbers">
                <p>pages:</p>
                <span style={page === 1 ? {color: '#3bff00'} : {}} onClick={() => changePage(1)}>1</span>
                <span style={page === 2 ? {color: '#3bff00'} : {}} onClick={() => changePage(2)}>2</span>
            </div>

        </div>
    );
}

export default Users;
