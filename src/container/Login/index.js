import React, {useState} from "react";
import TextField from "@material-ui/core/TextField";
import Grid from "@material-ui/core/Grid";
import './login.css';
import Box from "@material-ui/core/Box";
import Button from "@material-ui/core/Button";
import axios from "axios";


function Login({showError, showSpinner, hideSpinner, logIn}) {

    const init = {
        email: 'eve.holt@reqres.in',
        password: 'cityslicka',
    }
    const [form, setForm] = useState(init)
    const handleChange = (e) => {
        setForm({...form, [e.target.name]: e.target.value})
    }

    const submit = () => {
        showSpinner();
        axios.post('https://reqres.in/api/login', form)
            .then((res) => {
                logIn(res.data.token);
                hideSpinner();
            }).catch((err) => {
            showError(err);
            hideSpinner();
        });
    }


    return (
        <div className="login">
            <form className="login-container" noValidate autoComplete="off">
                <Grid container>
                    <Grid container justify="center" alignItems="center">
                        <Box mt={4}>
                            <TextField value={form.email} onChange={handleChange} name="email" label="Email"/>
                        </Box>
                    </Grid>
                    <Grid container justify="center" alignItems="center">
                        <TextField value={form.password} name="password" onChange={handleChange} label="Password"
                                   type="password"/>
                    </Grid>
                    <Grid container justify="center" alignItems="center">
                        <Box m={5}>
                            <Button onClick={submit} variant="contained" color="primary">
                                Login
                            </Button>
                        </Box>
                    </Grid>
                </Grid>
            </form>
        </div>
    );
}

export default Login;
