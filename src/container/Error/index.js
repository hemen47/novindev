import React from "react";
import {makeStyles} from "@material-ui/core";
import Button from "@material-ui/core/Button";

// styles in this component are CSS in JS using a syntax similar to Styled-Components:
const useStyles = makeStyles({
    container: {
        position: 'fixed',
        width: '400px',
        height: '200px,',
        top: '50%',
        left: '50%',
        marginTop: '-100px',
        marginLeft: '-200px',
        backgroundColor: "white",
        boxShadow: '2px 7px 20px -1px rgba(0,0,0,0.75)',
        borderRadius: '8px 8px 8px 8px',
        textAlign: "center"
    },
    title: {
        fontSize: '1rem',
        padding: '20px',
        margin: '0',
        color: "white",
        borderRadius: '8px 8px 0px 0px',
        backgroundColor: "#f50057"
    },
    button: {
        margin: '10px'
    }
});

function Error({hideError, errorMessage}) {
    const classes = useStyles();

    return (
        <div className={classes.container}>
            <h1 className={classes.title}>Error!</h1>
            <p className={classes.message}>{errorMessage}</p>
            <Button className={classes.button} variant="contained" color="secondary" onClick={hideError}>ok</Button>
        </div>
    );
}

export default Error;
